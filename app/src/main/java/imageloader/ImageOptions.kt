package imageloader

import android.content.Context

import com.bumptech.glide.Glide
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.module.GlideModule

/**
 * Created by D.V.N on 9/11/2016.
 */
class ImageOptions : GlideModule {

    override fun applyOptions(context: Context, builder: GlideBuilder) {
        // Apply options to the builder here.
        builder.setDecodeFormat(DecodeFormat.PREFER_ARGB_8888)
    }

    override fun registerComponents(context: Context, glide: Glide) {
        // register ModelLoaders here.
    }

}
