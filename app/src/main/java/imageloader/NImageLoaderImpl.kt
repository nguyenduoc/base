package imageloader

import android.content.Context
import android.graphics.Bitmap
import android.support.annotation.DrawableRes
import android.text.TextUtils
import android.widget.ImageView

import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.bumptech.glide.request.target.Target
import com.dvn.base.R

import java.util.concurrent.ExecutionException


/**
 * Created by D.V.N on 9/11/2016.
 */
class NImageLoaderImpl : NImageLoader {
    override fun loadBitmap(context: Context, view: ImageView, url: String, listener: ImageLoadingListener) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun loadBitmap(context: Context, url: String, listener: ImageLoadingListener, bitmapWidth: Int, bitmapHeight: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun display(view: ImageView, url: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun display(view: ImageView, url: String, @DrawableRes drawableResLoading: Int, @DrawableRes drawableResDefault: Int) {
        if (TextUtils.isEmpty(url)) return
        Glide.with(view.context)
                .load(url)
                .placeholder(drawableResLoading)
                .crossFade()
                .error(drawableResDefault)
                .into(view)
    }

    override fun display(view: ImageView, url: String, @DrawableRes drawableResDefault: Int) {
        display(view, url, DRAWABLE_RES_LOADING, drawableResDefault)
    }

    override fun displayGif(view: ImageView, url: String, @DrawableRes drawableResLoading: Int) {
        if (TextUtils.isEmpty(url)) return
        Glide.with(view.context)
                .load(url)
                .asGif()
                .placeholder(drawableResLoading)
                .crossFade()
                .into(view)
    }

    override fun displayGif(view: ImageView, url: String) {
        displayGif(view, url, DRAWABLE_RES_LOADING)
    }

    companion object {
        @DrawableRes
        private val DRAWABLE_RES_LOADING = R.drawable.ic_launcher
    }
}
