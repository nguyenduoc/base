package imageloader

import android.content.Context
import android.support.annotation.DrawableRes
import android.widget.ImageView

import imageloader.ImageLoadingListener

/**
 * Created by D.V.N on 9/11/2016.
 */
interface NImageLoader {

    fun display(view: ImageView, url: String, @DrawableRes drawableResLoading: Int, @DrawableRes drawableResDefault: Int)

    fun display(view: ImageView, url: String, @DrawableRes drawableResDefault: Int)

    fun displayGif(view: ImageView, url: String, @DrawableRes drawableResLoading: Int)

    fun displayGif(view: ImageView, url: String)

    fun loadBitmap(context: Context, view: ImageView, url: String, listener: ImageLoadingListener)

    fun loadBitmap(context: Context, url: String, listener: ImageLoadingListener, bitmapWidth: Int, bitmapHeight: Int)

    fun display(view: ImageView, url: String)
}
