package imageloader

import android.graphics.Bitmap

/**
 * Created by D.V.N on 9/11/2016.
 */
interface ImageLoadingListener {
    fun onLoadingStart()

    fun onLoadingError()

    fun onLoadingComplete(bitmap: Bitmap)

    fun onLoadingCancel()
}
