package views.recycleview

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup

import java.util.ArrayList

/**
 * Created by Smile on 9/26/2016.
 */

abstract class AppRecycleViewAdapter<T> : RecyclerView.Adapter<AppViewHolder> {
    private var mData: MutableList<T>? = null

    constructor() {
        mData = ArrayList<T>()
    }

    constructor(data: List<T>?) {
        mData = data as MutableList<T>? ?: ArrayList<T>()
    }


    override fun getItemCount(): Int {
        return if (mData != null) mData!!.size else 0
    }

    fun addData(item: T) {
        if (mData != null) {
            mData!!.add(item)
            notifyItemChanged(mData!!.size - 1)
        }
    }

    operator fun get(position: Int): T? {
        return if (mData == null) null else mData!![position]
    }
}
