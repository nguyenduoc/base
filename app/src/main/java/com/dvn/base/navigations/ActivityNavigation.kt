package com.dvn.base.navigations

import android.support.annotation.IdRes

import com.dvn.base.DActivity

/**
 * Created by D.V.N on 9/3/2016.
 */
class ActivityNavigation(private var mActivity: DActivity?, @IdRes contentLayout: Int)
    : NavigationController(mActivity?.supportFragmentManager, contentLayout) {

    override fun checkNavigation(): Boolean {
        return mActivity != null && mActivity!!.canShowPage()
    }

    override fun checkCanGoBack(): Boolean {
        return activePage != null
    }

    override fun destroy() {
        super.destroy()
        mActivity = null
    }
}
