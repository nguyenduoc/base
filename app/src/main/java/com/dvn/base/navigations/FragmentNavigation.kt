package com.dvn.base.navigations

import android.support.annotation.IdRes

import com.dvn.base.DFragment

/**
 * Created by D.V.N on 9/3/2016.
 */
class FragmentNavigation(private var mFragment: DFragment?, @IdRes contentLayout: Int) : NavigationController(mFragment?.childFragmentManager, contentLayout) {

    override fun checkNavigation(): Boolean {
        return mFragment != null && mFragment!!.canShowPage()
    }

    override fun checkCanGoBack(): Boolean {
        return activePage != null
    }

    override fun destroy() {
        super.destroy()
        mFragment = null
    }
}
