package com.dvn.base.navigations

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import com.dvn.base.R

/**
 * Created by D.V.N on 9/3/2016.
 */
abstract class NavigationController internal constructor(protected var mFragmentManager: FragmentManager?, var mContentLayout: Int) : INavigationController {

    override fun showPage(fragment: Fragment,  isAddToBackStack: Boolean) {
        if (!checkNavigation()) return
        val transaction = mFragmentManager!!.beginTransaction()
        transaction.replace(mContentLayout, fragment)
        if (isAddToBackStack)
            transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun goBack(): Boolean {
        if (!checkCanGoBack()) return false
        mFragmentManager!!.popBackStack()
        // Even popped back stack, the fragment which is added without addToBackStack would be showing.
        // So we need to remove it manually
        val transaction = mFragmentManager!!.beginTransaction()
        val currentFrag = mFragmentManager!!.findFragmentById(mContentLayout)
        if (currentFrag != null) {
            transaction.remove(currentFrag)
            transaction.commit()
        }
        return true
    }

    override fun showPage(fragment: Fragment) {
        showPage(fragment, true)
    }

    override fun goTag(tag: String) {
        if (!checkNavigation()) return
        mFragmentManager!!.popBackStack(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    override val activePage: Fragment?
        get() = mFragmentManager?.findFragmentById(mContentLayout)

    protected abstract fun checkNavigation(): Boolean

    protected abstract fun checkCanGoBack(): Boolean

    override fun destroy() {
        mFragmentManager = null
        mContentLayout = 0
    }
}
