package com.dvn.base.navigations

import android.support.v4.app.Fragment

/**
 * Created by D.V.N on 9/3/2016.
 */
interface INavigationController {
    fun showPage(fragment: Fragment, isAddToBackStack: Boolean)

    fun showPage(fragment: Fragment)

    fun goBack(): Boolean

    fun goTag(tag: String)

    val activePage: Fragment?

    fun destroy()
}
