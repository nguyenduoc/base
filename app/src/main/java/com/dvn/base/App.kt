package com.dvn.base

import android.app.Application

/**
 * Created by D.V.N on 9/11/2016.
 */
class App : Application() {
    companion object {
        private var sInstance: App? = null

        fun get(): App? {
            return sInstance
        }
    }

    override fun onCreate() {
        super.onCreate()
        sInstance = this
    }


}
