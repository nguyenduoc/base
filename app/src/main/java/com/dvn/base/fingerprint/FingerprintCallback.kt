package com.dvn.base.fingerprint

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.hardware.fingerprint.FingerprintManager
import android.os.Build
import android.os.CancellationSignal
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat

/**
 * Created by DVN on 4/15/2017.
 */

@RequiresApi(api = Build.VERSION_CODES.M)
abstract class FingerprintCallback(private val context: Context) : FingerprintManager.AuthenticationCallback() {


    fun startAuth(manager: FingerprintManager, cryptoObject: FingerprintManager.CryptoObject) {
        val cancellationSignal = CancellationSignal()
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return
        }
        manager.authenticate(cryptoObject, cancellationSignal, 0, this, null)
    }


    override fun onAuthenticationError(errMsgId: Int, errString: CharSequence) {
        this.onAuthentication("Fingerprint Authentication error\n" + errString, false)
    }


    override fun onAuthenticationHelp(helpMsgId: Int, helpString: CharSequence) {
        this.onAuthentication("Fingerprint Authentication help\n" + helpString, false)
    }


    override fun onAuthenticationFailed() {
        this.onAuthentication("Fingerprint Authentication failed.", false)
    }


    override fun onAuthenticationSucceeded(result: FingerprintManager.AuthenticationResult) {
        this.onAuthentication("Fingerprint Authentication succeeded.", true)
    }

    abstract fun onPermissionFingerPrintError()

    abstract fun onDeviceNotSupportFingerprint()

    abstract fun onMustEnrollFingerprints()


    abstract fun onAuthentication(e: String, success: Boolean?)
}
