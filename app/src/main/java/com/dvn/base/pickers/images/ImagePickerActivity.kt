package com.dvn.base.pickers.images

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SimpleItemAnimator
import android.view.View

import com.dvn.base.DActivity
import com.dvn.base.R
import com.dvn.base.permissions.PermissionChecker
import com.dvn.base.utils.DestroyUtils
import com.dvn.base.widgets.OnItemRecycleViewClickListener

import java.util.ArrayList

/**
 * Created by Smile on 9/26/2016.
 */

class ImagePickerActivity : DActivity() {
    private var mRecyclerView: RecyclerView? = null
    private var mAdapter: ImagePickerAdapter? = null
    private var mAsyncTack: AsyncTask<Void, Void, ArrayList<String>>? = null
    private var mPermissionChecker: PermissionChecker? = null
    private var mHandler: Handler? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picker_image)
        mRecyclerView = findViewById(R.id.recycle_view) as RecyclerView
        mRecyclerView!!.layoutManager = createLayoutManagerRecycleView()
        (mRecyclerView!!.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        mRecyclerView!!.addOnItemTouchListener(object : OnItemRecycleViewClickListener(this@ImagePickerActivity) {
            override fun onItemClick(view: View, position: Int) {
                if (mAdapter != null) mAdapter!!.currentSelectPos = position
                DestroyUtils.destroyHandler(mHandler!!)
                mHandler = Handler()
                mHandler!!.postDelayed({
                    val res = if (mAdapter != null && mAdapter!!.currentSelectPos >= 0)
                        mAdapter!!.get(mAdapter!!.currentSelectPos)
                    else
                        null
                    val resultIntent = Intent()
                    resultIntent.putExtra(EXTRA_IMAGE, res)
                    setResult(RESULT_OK, resultIntent)
                    finish()
                }, 500)

            }
        })
        if (mAdapter == null) {
            getDataFromMemory()
        } else {
            mRecyclerView!!.adapter = mAdapter
        }
    }

    private fun getDataFromMemory() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            val permissionCheck = ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                doGetDataFromMemory()
            } else {
                val listener = object : PermissionChecker.AddPermissionListener {
                    override fun onPermissionGranted(per: String) {
                        doGetDataFromMemory()
                    }

                    override fun onPermissionDenied(per: String) {

                    }
                }
                mPermissionChecker = PermissionChecker(this)
                mPermissionChecker!!.addPermission(Manifest.permission.READ_EXTERNAL_STORAGE, listener)
                mPermissionChecker!!.checkPermission()
            }
        } else {
            doGetDataFromMemory()
        }
    }

    private fun doGetDataFromMemory() {
        if (mAsyncTack != null) mAsyncTack!!.cancel(true)
        mAsyncTack = object : AsyncTask<Void, Void, ArrayList<String>>() {
            override fun doInBackground(vararg params: Void): ArrayList<String>? {
                return ImagePickerUtils.getAllShownImagesPath(applicationContext)
            }

            override fun onPostExecute(strings: ArrayList<String>?) {
                super.onPostExecute(strings)
                if (strings != null) {
                    mAdapter = ImagePickerAdapter(strings)
                    if (mRecyclerView != null) mRecyclerView!!.adapter = mAdapter
                }
            }
        }
        mAsyncTack!!.execute()
    }

    private fun createLayoutManagerRecycleView(): RecyclerView.LayoutManager {
        return GridLayoutManager(this, 4)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mAsyncTack != null) {
            mAsyncTack!!.cancel(true)
        }
    }

    companion object {
        val EXTRA_IMAGE = "extra_image"
    }
}
