package com.dvn.base.pickers.images

import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore

import java.util.ArrayList

/**
 * Created by Smile on 9/27/2016.
 */

internal object ImagePickerUtils {
    fun getAllShownImagesPath(context: Context?): ArrayList<String>? {
        if (context == null) return null

        val uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        val projection = arrayOf(MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME)
        val cursor = context.contentResolver.query(uri, projection, null, null, null) ?: return null

        val listOfAllImages = ArrayList<String>()
        var absolutePathOfImage: String
        val columnIndexData = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA)
        while (cursor.moveToNext()) {
            absolutePathOfImage = cursor.getString(columnIndexData)
            listOfAllImages.add(absolutePathOfImage)
        }
        cursor.close()
        return listOfAllImages
    }
}
