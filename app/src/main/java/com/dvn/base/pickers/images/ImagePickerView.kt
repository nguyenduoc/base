package com.dvn.base.pickers.images

import android.content.Context
import android.widget.FrameLayout
import android.widget.ImageView

import com.dvn.base.R
import com.dvn.base.utils.ImageUtils

/**
 * Created by Smile on 9/26/2016.
 */

internal class ImagePickerView(context: Context) : FrameLayout(context) {
    private val mPictureView: ImageView
    private val mCheckerView: ImageView

    init {
        inflate(context, R.layout.include_item_image_picker, this)
        mPictureView = findViewById(R.id.picture) as ImageView
        mCheckerView = findViewById(R.id.checker) as ImageView
    }

    fun display(image: String, isSelect: Boolean) {
        ImageUtils.display(mPictureView, image)
        mCheckerView.visibility = if (isSelect) VISIBLE else INVISIBLE
    }

}
