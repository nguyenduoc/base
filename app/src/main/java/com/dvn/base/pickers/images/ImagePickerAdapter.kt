package com.dvn.base.pickers.images

import android.view.ViewGroup

import views.recycleview.AppRecycleViewAdapter
import views.recycleview.AppViewHolder

/**
 * Created by Smile on 9/26/2016.
 */

internal class ImagePickerAdapter(data: List<String>) : AppRecycleViewAdapter<String>(data) {
    var currentSelectPos = -1
        set(position) {
            val pos = currentSelectPos
            field = position
            notifyItemChanged(currentSelectPos)
            if (pos >= 0) notifyItemChanged(pos)
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppViewHolder {
        return AppViewHolder(ImagePickerView(parent.context))
    }

    override fun onBindViewHolder(holder: AppViewHolder, position: Int) {
        (holder.itemView as ImagePickerView).display(get(position)!!, position == currentSelectPos)
    }
}
