package com.dvn.base.activitys

import android.os.Bundle
import com.dvn.base.DActivity
import com.dvn.base.screens.home.HomeFragment

class MainActivity : DActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (mNavigationController?.activePage == null) {
            mNavigationController?.showPage(HomeFragment(), false)
        }
    }
}
