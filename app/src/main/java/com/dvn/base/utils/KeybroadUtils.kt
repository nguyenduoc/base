package com.dvn.base.utils

import android.content.Context
import android.view.inputmethod.InputMethodManager
import android.widget.EditText

import com.dvn.base.App

/**
 * Created by DVN on 4/5/2017.
 */

object KeybroadUtils {
    fun showKeyboard(editText: EditText) {
        val imm = App.get()!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
    }

    fun hideKeyboard(editText: EditText) {
        val imm = App.get()!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(editText.windowToken, 0)
    }
}