package com.dvn.base.utils

import android.os.Handler

import com.dvn.base.navigations.INavigationController

object DestroyUtils {

    fun destroyHandler(vararg handlers: Handler) {
        for (handler in handlers) {
            handler.removeCallbacksAndMessages(null)
        }
    }

    fun destroyNavigationController(navigationController: INavigationController?) {
        navigationController?.destroy()
    }
}
