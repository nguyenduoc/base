package com.dvn.base.utils;

import com.dvn.base.api.responses.AppResponse;
import com.google.gson.Gson;

/**
 * Created by freesky1102 on 3/9/16.
 */
public class GsonUtils {
    private static final Gson GSON_PARSER = new Gson();

    public static <T extends AppResponse> T parse(String jsonText, Class<T> objectClass) {
        return GSON_PARSER.fromJson(jsonText, objectClass);
    }


}
