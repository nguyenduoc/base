package com.dvn.base.utils

import android.support.annotation.StringRes
import android.widget.Toast

import com.dvn.base.App

/**
 * Created by DVN on 3/6/2017.
 */

object ToastUtils {
    fun show(message: String) {
        Toast.makeText(App.get(), message, Toast.LENGTH_SHORT).show()
    }

    fun show(@StringRes messageId: Int) {
        show(App.get()!!.getString(messageId))
    }

    fun showDebug(message: String) {
        if (AppLog.isEnabled)
            show(message)
    }
}
