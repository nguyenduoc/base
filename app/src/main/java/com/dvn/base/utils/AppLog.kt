package com.dvn.base.utils

import android.util.Log

/**
 * Created by D.V.N on 9/16/2016.
 */
object AppLog {
    val isEnabled = true

    private val TAG = "D.V.N"

    fun d(message: String?) {
        if (message == null) return
        if (isEnabled)
            Log.d(TAG, message)
    }

    fun e(message: String?) {
        if (message == null) return
        if (isEnabled)
            Log.e(TAG, message)
    }

    fun i(message: String?) {
        if (message == null) return
        if (isEnabled)
            Log.i(TAG, message)
    }

    fun w(message: String?) {
        if (message == null) return
        if (isEnabled)
            Log.w(TAG, message)
    }
}
