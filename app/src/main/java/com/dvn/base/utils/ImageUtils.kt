package com.dvn.base.utils

import android.widget.ImageView

import imageloader.NImageLoader
import imageloader.NImageLoaderImpl

/**
 * Created by Smile on 9/27/2016.
 */

object ImageUtils {
    private val mImageLoader = NImageLoaderImpl()

    fun display(view: ImageView, urlMemory: String) {
        mImageLoader.display(view, urlMemory)
    }
}
