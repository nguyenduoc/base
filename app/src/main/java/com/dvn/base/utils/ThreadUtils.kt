package com.dvn.base.utils

import android.os.Looper

/**
 * Created by DVN on 3/6/2017.
 */

object ThreadUtils {
    val isMainThread: Boolean
        get() = Looper.getMainLooper().thread === Thread.currentThread()
}
