package com.dvn.base.api

import com.dvn.base.api.responses.SignInResponse
import com.squareup.okhttp.Callback

/**
 * Created by DVN on 3/1/2017.
 */

class ApiImpl : Api {
    override fun signIn(username: String, password: String, callback: Callback) {
        AppRequest().makeRequest(SignInResponse.url, ApiParamRequest.createParamSignIn(username, password), callback)
    }
}
