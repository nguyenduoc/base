package com.dvn.base.api.dfe

import android.os.Handler

import com.dvn.base.api.Api
import com.dvn.base.api.OnResponseListener
import com.dvn.base.utils.AppLog
import com.dvn.base.utils.DestroyUtils
import com.dvn.base.utils.GsonUtils
import com.dvn.base.utils.ThreadUtils
import com.squareup.okhttp.Callback
import com.squareup.okhttp.Request
import com.squareup.okhttp.Response
import com.squareup.okhttp.internal.NamedRunnable

import java.io.IOException
import java.util.HashSet

/**
 * Created by DVN on 3/3/2017.
 */

abstract class DfeModel<T> internal constructor(internal var mApi: Api) : Callback {

    private val mListeners: MutableSet<OnResponseListener<T>>?
    private val mHandler: Handler

    init {
        this.mListeners = HashSet<OnResponseListener<T>>()
        this.mHandler = Handler()
    }

    fun addOnResponseListener(listener: OnResponseListener<T>) {
        mListeners!!.add(listener)
    }

    internal fun notifyDataSetChanged(data: T) {
        if (mListeners == null) return
        mHandler.post {
            for (listener in mListeners) {
                listener?.onResponseSuccess(data)
            }
        }
    }

    override fun onFailure(request: Request?, e: IOException?) {
        AppLog.e(e.toString())
    }

    fun destroy() {
        mListeners?.clear()

        DestroyUtils.destroyHandler(mHandler)
    }
}
