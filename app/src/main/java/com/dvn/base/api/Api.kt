package com.dvn.base.api

import com.squareup.okhttp.Callback

/**
 * Created by DVN on 3/1/2017.
 */

interface Api {
    fun signIn(username: String, password: String, callback: Callback)
}
