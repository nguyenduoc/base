package com.dvn.base.api

import java.util.HashMap

/**
 * Created by DVN on 3/6/2017.
 */

internal object ApiParamRequest {
    fun createBaseParam(): MutableMap<String, String> {
        val params = HashMap<String, String>()
        params.put(ApiKey.APP_NAME, "Base")
        return params
    }

    fun createParamSignIn(username: String, password: String): Map<String, String> {
        val params = createBaseParam()
        params.put(ApiKey.USERNAME, username)
        params.put(ApiKey.PASSWORD, password)
        return params
    }
}
