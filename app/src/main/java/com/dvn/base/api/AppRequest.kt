package com.dvn.base.api

import com.squareup.okhttp.Callback
import com.squareup.okhttp.HttpUrl
import com.squareup.okhttp.OkHttpClient
import com.squareup.okhttp.Request
import java.util.concurrent.TimeUnit

/**
 * Created by DVN on 3/1/2017.
 */

internal class AppRequest {
    private val mOkHttpClient: OkHttpClient?

    init {
        mOkHttpClient = OkHttpClient()
        mOkHttpClient.setReadTimeout(5000, TimeUnit.MILLISECONDS)
    }

    fun makeRequest(url: String, params: Map<String, String>?, callback: Callback) {
        val urlBuilder = HttpUrl.parse(url).newBuilder()

        for ((key, value) in params!!) {
            urlBuilder.addQueryParameter(key, value)
        }

        val request = Request.Builder().url(urlBuilder.build().toString()) .build()
        mOkHttpClient!!.newCall(request).enqueue(callback)

    }

    fun destroy() {
        mOkHttpClient?.cancel(null)
    }
}
