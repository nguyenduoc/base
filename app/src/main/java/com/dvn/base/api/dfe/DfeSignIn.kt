package com.dvn.base.api.dfe

import com.dvn.base.api.Api
import com.dvn.base.api.responses.SignInResponse
import com.dvn.base.utils.GsonUtils
import com.squareup.okhttp.Response
import java.io.IOException

/**
 * Created by DVN on 3/6/2017.
 */

class DfeSignIn(api: Api) : DfeModel<SignInResponse>(api) {

    fun startRequest(username: String, password: String) {
        this.mApi.signIn(username, password, this)
    }

    @Throws(IOException::class)
    override fun onResponse(response: Response) {
        if (!response.isSuccessful) {
            throw IOException("Unexpected code " + response)
        }
        val jsonString = response.body().string()
        notifyDataSetChanged(GsonUtils.parse(jsonString, SignInResponse::class.java))
    }
}
