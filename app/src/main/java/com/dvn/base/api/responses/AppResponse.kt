package com.dvn.base.api.responses

import com.dvn.base.api.Config

/**
 * Created by DVN on 3/3/2017.
 */

open class AppResponse {
    companion object {
        fun makePath(subPath: String): String {
            return Config.SERVER_ID + subPath
        }
    }
}
