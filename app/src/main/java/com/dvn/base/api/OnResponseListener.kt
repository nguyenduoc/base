package com.dvn.base.api

/**
 * Created by DVN on 3/6/2017.
 */

interface OnResponseListener<in T> {
    fun onResponseSuccess(data: T)

    fun onResponseError()
}
