package com.dvn.base.api.responses

import com.dvn.base.api.ApiKey.VERSION
import com.dvn.base.api.ApiSubPath
import com.google.gson.annotations.SerializedName

/**
 * Created by DVN on 3/6/2017.
 */

class SignInResponse : AppResponse() {
    @SerializedName(VERSION)
    private var version: Int = 0

    companion object {
        val url: String
            get() = AppResponse.makePath(ApiSubPath.PATH_SIGN_IN)
    }
}
