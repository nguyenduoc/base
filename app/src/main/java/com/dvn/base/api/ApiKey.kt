package com.dvn.base.api

/**
 * Created by DVN on 3/6/2017.
 */

object ApiKey {
    const val NAME = "name"
    const val APP_NAME = "app_name"
    const val USERNAME = "username"
    const val PASSWORD = "password"
    const val VERSION = "version"
}
