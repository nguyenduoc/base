package com.dvn.base

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.os.Build
import android.support.annotation.RequiresApi
import android.util.AttributeSet
import android.view.View

/**
 * Created by DVN on 4/9/2017.
 */

class LoadingView : View {
    companion object {
        private val DEFAULT_TOP_COLOR = Color.parseColor("#FF2FBA00")
        private val DEFAULT_BOTTOM_COLOR = Color.parseColor("#FFFF6600")
        private val DEFAULT_SHADOW_COLOR = Color.parseColor("#b8b8b8")
        private val DEFAULT_WIDTH: Int = App.get()!!.resources.getDimensionPixelSize(R.dimen.dimen_weight_loading_view)


        private val DEFAULT_SHADOW = 2
        private val DEFAULT_SPEED_OF_DEGREE = 10
    }

    private var width: Int? = 0
    private var arc: Int = 0
    private var topDegree = 10
    private var bottomDegree = 190
    private var loadingRectF: RectF? = null
    private var shadowRectF: RectF? = null
    private var speedOfDegree: Int = 0
    private var speedOfArc: Float = 0.toFloat()
    private var shadow: Int = 0
    private var paint: Paint? = null
    private var changeBigger = true
    private var topColor: Int = 0
    private var bottomColor: Int = 0
    private var shadowColor: Int = 0

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs)
    }


    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        speedOfDegree = DEFAULT_SPEED_OF_DEGREE
        speedOfArc = (DEFAULT_SPEED_OF_DEGREE / 4).toFloat()
        width = DEFAULT_WIDTH
        shadow = DEFAULT_SHADOW
        topColor = DEFAULT_TOP_COLOR
        bottomColor = DEFAULT_BOTTOM_COLOR
        shadowColor = DEFAULT_SHADOW_COLOR

        paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint!!.style = Paint.Style.STROKE
        paint!!.strokeWidth = width!!.toFloat()
        paint!!.strokeCap = Paint.Cap.ROUND
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        arc = 10
        val dupleWidth = 2 * width!!
        loadingRectF = RectF(dupleWidth.toFloat(), dupleWidth.toFloat(), (w - dupleWidth).toFloat(), (h - dupleWidth).toFloat())
        shadowRectF = RectF((dupleWidth + shadow).toFloat(), (dupleWidth + shadow).toFloat(), (w - dupleWidth + shadow).toFloat(), (h - dupleWidth + shadow).toFloat())
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        paint!!.color = shadowColor
        canvas.drawArc(shadowRectF!!, topDegree.toFloat(), arc.toFloat(), false, paint!!)
        canvas.drawArc(shadowRectF!!, bottomDegree.toFloat(), arc.toFloat(), false, paint!!)

        paint!!.color = topColor
        canvas.drawArc(loadingRectF!!, topDegree.toFloat(), arc.toFloat(), false, paint!!)
        paint!!.color = bottomColor
        canvas.drawArc(loadingRectF!!, bottomDegree.toFloat(), arc.toFloat(), false, paint!!)

        topDegree += speedOfDegree
        bottomDegree += speedOfDegree

        if (topDegree > 360) {
            topDegree -= 360
        }

        if (bottomDegree > 360) {
            bottomDegree -= 360
        }

        var isValidate = false

        if (changeBigger) {
            if (arc < 160) {
                arc += speedOfArc.toInt()
                isValidate = true
            }
        } else {
            if (arc > speedOfDegree) {
                arc -= (2 * speedOfArc).toInt()
                isValidate = true
            }
        }

        if (arc >= 160 || arc <= 10) {
            changeBigger = !changeBigger
            isValidate = true
        }

        if (isValidate) {
            invalidate()
        }
    }
}
