package com.dvn.base.widgets

import android.content.Context
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.view.View

/**
 * Created by Smile onFlash 3/25/2016.
 */
class RecycleViewGridLayoutManager : GridLayoutManager {
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {}

    constructor(context: Context, spanCount: Int) : super(context, spanCount) {}


    constructor(context: Context, spanCount: Int, orientation: Int, reverseLayout: Boolean) : super(context, spanCount, orientation, reverseLayout) {}

    override fun onMeasure(recycler: RecyclerView.Recycler?, state: RecyclerView.State?, widthSpec: Int, heightSpec: Int) {
        var height = 0
        val childCount = itemCount
        for (i in 0..childCount - 1) {
            val child = recycler!!.getViewForPosition(i)
            measureChild(child, widthSpec, heightSpec)
            if (i % spanCount == 0) {
                val measuredHeight = child.measuredHeight + getDecoratedBottom(child)
                height += measuredHeight
            }
        }
        setMeasuredDimension(View.MeasureSpec.getSize(widthSpec), height)
    }
}
