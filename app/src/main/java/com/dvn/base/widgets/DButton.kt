package com.dvn.base.widgets

import android.content.Context
import android.support.v7.widget.AppCompatButton
import android.util.AttributeSet

import com.dvn.base.audios.soundpool.SoundController
import com.dvn.base.utils.AppLog

/**
 * Created by DVN on 4/7/2017.
 */

class DButton : AppCompatButton {
    private val hasSound = true

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}

    override fun performClick(): Boolean {
        if (hasSound) {
            SoundController.get()!!.play()
        }
        return super.performClick()
    }
}
