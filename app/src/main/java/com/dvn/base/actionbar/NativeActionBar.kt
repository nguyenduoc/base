package com.dvn.base.actionbar

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.view.View
import android.widget.RelativeLayout

import com.dvn.base.DFragment
import com.dvn.base.R

/**
 * Created by D.V.N on 9/4/2016.
 */
class NativeActionBar : RelativeLayout, CustomActionBar {
    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(context)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
    }

    private fun init(context: Context) {

    }

    override fun syncActionBar(fragment: DFragment) {
        val layoutRes = findLayoutInflate(fragment)
        if (layoutRes == 0) return
        inflate(context, layoutRes, this)
    }

    private fun findLayoutInflate(fragment: DFragment): Int {
        setBackgroundResource(R.color.colorPrimary)
        return R.layout.action_bar_default
    }
}
