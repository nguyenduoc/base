package com.dvn.base.actionbar

import com.dvn.base.DFragment

/**
 * Created by D.V.N on 9/4/2016.
 */
interface CustomActionBar {
    fun syncActionBar(fragment: DFragment)

}
