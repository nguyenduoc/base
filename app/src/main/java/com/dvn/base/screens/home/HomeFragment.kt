package com.dvn.base.screens.home

import android.os.Bundle
import android.view.View
import com.dvn.base.DFragment
import com.dvn.base.R
import kotlinx.android.synthetic.main.fragment_home.*

/**
 * Created by Smile on 10/7/2016.
 */

class HomeFragment : DFragment(), HomeView {
    override val layoutRes: Int
        get() = R.layout.fragment_home

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mClick.setOnClickListener({
            mRootController?.showPage(HomeFragment())
        })

    }
}
