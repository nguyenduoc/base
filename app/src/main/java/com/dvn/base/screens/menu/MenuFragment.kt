package com.dvn.base.screens.menu

import com.dvn.base.DFragment
import com.dvn.base.R

/**
 * Created by DVN on 3/8/2017.
 */

class MenuFragment : DFragment() {
    protected fun creatingPresenter() {

    }

    override val layoutRes: Int
        get() = R.layout.fragment_menu
}
