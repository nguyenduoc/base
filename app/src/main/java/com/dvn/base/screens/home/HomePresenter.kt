package com.dvn.base.screens.home

import android.os.Bundle

import com.dvn.base.screens.precenters.IPresenter

/**
 * Created by Smile on 10/11/2016.
 */

class HomePresenter(private val mHomeView: HomeView) : IPresenter, HomeView {

    override fun getDataFromBundle(bundle: Bundle) {

    }

    override fun saveDataToBundle(bundle: Bundle) {

    }
}
