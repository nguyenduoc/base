package com.dvn.base.screens.precenters

import android.os.Bundle

/**
 * Created by D.V.N on 9/25/2016.
 */

interface IPresenter {

    fun getDataFromBundle(bundle: Bundle)

    fun saveDataToBundle(bundle: Bundle)
}
