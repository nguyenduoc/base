package com.dvn.base.screens.permission

import android.os.Bundle
import android.view.View
import android.widget.TextView

import com.dvn.base.DDialog
import com.dvn.base.R
import com.dvn.base.utils.Util

/**
 * Created by DVN on 4/9/2017.
 */

class PermissionDialog : DDialog() {

    override val layoutRes: Int
        get() = R.layout.dialog_permission


    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (view!!.findViewById(R.id.dialog_message) as TextView).text = getString(R.string.dialog_permission_message, getString(R.string.app_name))
        view.findViewById(R.id.ok).setOnClickListener { Util.openPermissionSetting(context) }
    }
}
