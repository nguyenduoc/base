package com.dvn.base.screens.models

/**
 * Created by D.V.N on 9/25/2016.
 */

interface IModel{
    fun destroy()
}
