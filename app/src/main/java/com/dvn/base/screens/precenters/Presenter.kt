package com.dvn.base.screens.precenters

import android.content.Context
import android.os.Bundle

import com.dvn.base.navigations.INavigationController
import com.dvn.base.screens.models.IModel
import com.dvn.base.screens.views.IView

/**
 * Created by DVN on 4/8/2017.
 */

open class Presenter<V : IView, M : IModel> : IPresenter {
    override fun saveDataToBundle(bundle: Bundle) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getDataFromBundle(bundle: Bundle) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    /**
     * Controller of activity
     */
    protected var mRootController: INavigationController? = null
    protected lateinit var mContext: Context
    protected var mModel: M? = null
    protected var mView: V? = null

    constructor() {

    }

    constructor(context: Context) {
        this.mContext = context
    }

    constructor(controller: INavigationController) {
        this.mRootController = controller
    }

    constructor(context: Context, controller: INavigationController) {
        this.mRootController = controller
        this.mContext = context
    }

    constructor(context: Context, view: V) {
        this.mView = view
        this.mContext = context
    }

    constructor(controller: INavigationController, view: V) {
        this.mView = view
        this.mRootController = controller
    }

    constructor(context: Context, controller: INavigationController, view: V) {
        this.mRootController = controller
        this.mContext = context
        this.mView = view
    }

    fun onGetDataFromBundle(bundle: Bundle) {

    }

    fun onSaveInstanceState(bundle: Bundle) {

    }

    fun onCreate() {

    }

    fun onViewCreated() {

    }

    open fun onActivityCreated() {

    }

    fun onStart() {

    }

    fun onResume() {

    }

    fun onPause() {

    }

    fun onStop() {

    }

    fun onDestroyView() {
        if (mModel != null) {
            mModel!!.destroy()
        }
    }

    fun onDestroy() {
        mRootController = null
        mView = null
        mModel = null
    }
}
