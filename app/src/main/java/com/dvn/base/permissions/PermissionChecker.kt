package com.dvn.base.permissions

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat

import java.util.ArrayList
import java.util.HashMap


/**

 * class to check permission added in [PermissionChecker.addPermission]
 * and call [ActivityCompat.requestPermissions]
 * to get Permission

 */
class PermissionChecker
/**

 * @param mContext must be [Activity]
 */
(private var mContext: Context?) {
    private var permissions: MutableMap<String, AddPermissionListener>? = HashMap()

    /**
     * add permission to check and run callback

     * @param permission to check
     * *
     * @param listener call back after check and request permission
     */
    fun addPermission(permission: String, listener: AddPermissionListener) {
        if (!permissions!!.containsKey(permission)) {
            permissions!!.put(permission, listener)
        }
    }


    /**
     * start call check Permission
     */
    fun checkPermission() {
        if (Build.VERSION_CODES.M <= Build.VERSION.SDK_INT) {
            check()
        } else {
            for (permission in permissions!!.keys) {
                val listener = permissions!![permission]
                listener?.onPermissionGranted(permission)
            }
        }
    }


    /**
     * check whether if Permission is granted or denied
     * and call [ActivityCompat.requestPermissions]
     */
    private fun check() {
        val per = ArrayList<String>()

        for (permission in permissions!!.keys) {
            val listener = permissions!![permission]
            if (ContextCompat.checkSelfPermission(mContext!!, permission) != PackageManager.PERMISSION_GRANTED) {
                per.add(permission)
            } else {
                listener?.onPermissionGranted(permission)
            }
        }

        if (per.size > 0) {
            val arr = arrayOfNulls<String>(per.size)
            ActivityCompat.requestPermissions((mContext as Activity?)!!,
                    per.toTypedArray(),
                    REQUEST_CODE_GET_PERMISSION)
        }
    }


    /**
     * call in [Activity.onRequestPermissionsResult]
     * to recieve results and call listener [AddPermissionListener]

     * @param requestCode
     * *
     * @param pers
     * *
     * @param grantResults
     */
    fun onRequestPermissionsResult(requestCode: Int,
                                   pers: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_CODE_GET_PERMISSION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0) {
                    for (i in pers.indices) {
                        val permission = pers[i]
                        val listener = permissions!![permission]
                        if (listener != null) {
                            if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                                // permission was granted, yay! Do the
                                // contacts-related task you need to do.
                                listener.onPermissionGranted(permission)
                            } else {

                                // permission denied, boo! Disable the
                                // functionality that depends on this permission.
                                listener.onPermissionDenied(permission)
                            }
                        }
                    }
                }
            }
        }
    }

    fun destroy() {
        mContext = null
        permissions!!.clear()
        permissions = null
    }


    interface AddPermissionListener {
        fun onPermissionGranted(per: String)

        fun onPermissionDenied(per: String)
    }

    companion object {
        val REQUEST_CODE_GET_PERMISSION = 112
    }
}
