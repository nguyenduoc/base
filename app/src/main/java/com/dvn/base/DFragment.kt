package com.dvn.base

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dvn.base.actionbar.CustomActionBar
import com.dvn.base.navigations.INavigationController

open class DFragment : Fragment() {
    /**
     * check activity has save instance

     * @return true if activity has save instance state or if false not save instance
     */
    protected var isSaveInstanceState: Boolean = false
    protected var mActionBar: CustomActionBar? = null
    protected var mRootController: INavigationController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isSaveInstanceState = false
        getDataFromBundle(savedInstanceState ?: arguments)
        if (hasRootController()) {
            val activity = activity
            if (activity != null && activity is DActivity) {
                mRootController = activity.mNavigationController
            }
        }
    }

    protected fun getDataFromBundle(bundle: Bundle?) {
        // NOTHING
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (isHasActionBar) return doCreateViewHasActionBar(inflater, container)
        return inflater.inflate(layoutRes, container, false)
    }

    private fun doCreateViewHasActionBar(inflater: LayoutInflater, container: ViewGroup?): View {
        val viewGroup = inflater.inflate(R.layout.fragment_has_action_bar, container, false) as ViewGroup
        inflater.inflate(layoutRes, viewGroup, true)
        mActionBar = viewGroup.findViewById(R.id.action_bar) as CustomActionBar
        mActionBar?.syncActionBar(this)
        return viewGroup
    }

    override fun onStart() {
        super.onStart()
        isSaveInstanceState = false
    }

    override fun onResume() {
        super.onResume()
        isSaveInstanceState = false
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        isSaveInstanceState = true
        super.onSaveInstanceState(outState)
    }

    fun canShowPage(): Boolean {
        return !isSaveInstanceState && isAdded
    }

    val isHasActionBar: Boolean
        get() = true

    open val layoutRes: Int
        @LayoutRes
        get() = 0

    fun hasRootController(): Boolean {
        return true
    }

}
