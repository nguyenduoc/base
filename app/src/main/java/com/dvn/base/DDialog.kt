package com.dvn.base

import android.app.Dialog
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window

/**
 * Created by DVN on 3/21/2017.
 */
open class DDialog : DialogFragment() {
    /**
     * check activity has save instance

     * @return true if activity has save instance state or if false not save instance
     */
    protected var isSaveInstanceState: Boolean = false
        private set

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isSaveInstanceState = false
        getDataFromBundle(savedInstanceState ?: arguments)
    }


    protected fun getDataFromBundle(bundle: Bundle) {
        // NOTHING
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(layoutRes, container, false)
    }

    override fun onStart() {
        super.onStart()
        isSaveInstanceState = false

        val dialog = dialog
        if (dialog == null || !isRemoveBlackBackground) return

        val window = dialog.window ?: return

        window.setBackgroundDrawableResource(android.R.color.transparent)
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        window.setDimAmount(0.5f)
    }

    val isRemoveBlackBackground: Boolean
        get() = true

    override fun onResume() {
        super.onResume()
        isSaveInstanceState = false
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        isSaveInstanceState = true
        super.onSaveInstanceState(outState)
    }

    fun canShowPage(): Boolean {
        return !isSaveInstanceState && isAdded
    }

    open val layoutRes: Int
        @LayoutRes
        get() = 0

}