package com.dvn.base.audios

import android.media.MediaPlayer
import android.os.Handler

import com.dvn.base.App
import com.dvn.base.utils.AppLog
import com.dvn.base.utils.DestroyUtils

import java.io.File
import java.io.IOException

/**
 * Created by D.V.N on 9/8/2016.
 * class play audio offline
 */
class Audio : IAudio, MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener {
    private var mMediaPlayer: MediaPlayer? = null
    override var isPlaying = false
        private set
    override var isStarted = false
        private set
    private var mOnAudioListener: OnAudioListener? = null
    private var mUrl: String? = null
    private var mAudioPlayingListener: AudioPlayingListener? = null
    private var mHandler: Handler? = null
    private var mAudioPreparedListener: AudioPreparedListener? = null

    constructor(url: String, onAudioListener: OnAudioListener) : this(url) {
        mOnAudioListener = onAudioListener
    }

    constructor(onAudioListener: OnAudioListener) {
        mOnAudioListener = onAudioListener
    }

    constructor(url: String) {
        mUrl = url
        mMediaPlayer = MediaPlayer()
        configAudio(mMediaPlayer!!)
        isStarted = false
        isPlaying = false

    }

    constructor() {
        mMediaPlayer = MediaPlayer()
        configAudio(mMediaPlayer!!)
        isStarted = false
        isPlaying = false

    }

    private fun configAudio(mediaPlayer: MediaPlayer) {
        mediaPlayer.setOnPreparedListener(this)
        mediaPlayer.setOnCompletionListener(this)
    }

    override fun prepared() {
        val file = AudioUtils.sanitizeDirectory(mUrl)
        if (file == null || !file.exists()) {
            AppLog.e("Audio: file not found... ($mUrl)")
            return
        }
        try {
            mMediaPlayer!!.reset()
            mMediaPlayer!!.setDataSource(file.absolutePath)
            mMediaPlayer!!.prepare()
        } catch (e: IOException) {
            mMediaPlayer = null
            AppLog.e("Audio: DataSource has exception... ($e)")
        }

    }

    override fun addPreparedListener(listener: AudioPreparedListener) {
        mAudioPreparedListener = listener
    }

    override fun setDataSource(url: String) {
        mUrl = url
    }

    override fun start() {
        if (mMediaPlayer == null) return
        val file = AudioUtils.sanitizeDirectory(mUrl)
        if (file == null || !file.exists()) {
            AppLog.e("Audio: file not found... ($mUrl)")
            return
        }
        try {
            isPlaying = true
            isStarted = true
            if (mOnAudioListener != null) mOnAudioListener!!.onAudioStart()
            mMediaPlayer!!.reset()
            mMediaPlayer!!.setDataSource(file.absolutePath)
            mMediaPlayer!!.prepare()
        } catch (e: IOException) {
            mMediaPlayer = null
            AppLog.e("Audio: DataSource has exception... ($e)")
        }

    }

    override fun start(url: String) {
        mUrl = url
        start()
    }


    override fun startFromRaw(rawRes: Int) {
        if (rawRes == 0) return
        mMediaPlayer = MediaPlayer.create(App.get(), rawRes)
        configAudio(mMediaPlayer!!)
        try {
            isPlaying = true
            isStarted = true
            if (mOnAudioListener != null) mOnAudioListener!!.onAudioStart()
            mMediaPlayer!!.start()
        } catch (e: IllegalStateException) {
            AppLog.e("Audio: startFromRaw file not found... ($e)")
        }

    }

    override fun pause() {
        isPlaying = false
        if (mMediaPlayer != null) mMediaPlayer!!.pause()
        if (mOnAudioListener != null) mOnAudioListener!!.onAudioPause()
    }

    override fun resume() {
        if (mMediaPlayer != null) mMediaPlayer!!.start()
        isPlaying = true
        if (mOnAudioListener != null) mOnAudioListener!!.onAudioResume()
        startListenerPlaying()
    }

    override fun stop() {
        if (mMediaPlayer == null) return
        if (mOnAudioListener != null) mOnAudioListener!!.onAudioStop()
        try {
            isPlaying = false
            mMediaPlayer!!.stop()
        } catch (e: IllegalStateException) {
            AppLog.e("Audio: stop has exception... ($e)")
        }

    }

    override fun destroy() {
        DestroyUtils.destroyHandler(mHandler!!)
        if (mMediaPlayer == null) return
        mUrl = null
        mMediaPlayer!!.release()
        isPlaying = false
        isStarted = false
        mMediaPlayer = null

    }

    override val duration: Int
        get() = if (mMediaPlayer != null) mMediaPlayer!!.duration else 0


    override fun onPrepared(mediaPlayer: MediaPlayer) {
        if (mAudioPreparedListener != null)
            mAudioPreparedListener!!.onAudioPreparedFinish()
        mediaPlayer.start()
        startListenerPlaying()
    }

    private fun startListenerPlaying() {
        if (mAudioPlayingListener != null) {
            countTime()
        }
    }

    override fun onCompletion(mediaPlayer: MediaPlayer) {
        isPlaying = false
        if (mOnAudioListener != null) mOnAudioListener!!.onAudioEnd()
    }

    override fun setAudioPlayingListener(listener: AudioPlayingListener) {
        mAudioPlayingListener = listener
    }

    private fun countTime() {
        DestroyUtils.destroyHandler(mHandler!!)
        mHandler = Handler()
        mHandler!!.postDelayed(countTimeRunnable, 100)
    }

    private val countTimeRunnable = Runnable {
        if (mMediaPlayer == null || mAudioPlayingListener == null || !isPlaying) {
            DestroyUtils.destroyHandler(mHandler!!)
            return@Runnable
        }
        mAudioPlayingListener!!.playing(mMediaPlayer!!.currentPosition)
        countTime()
    }

    override fun setCurrentPosition(position: Int) {
        if (mMediaPlayer != null) mMediaPlayer!!.seekTo(position)
    }
}
