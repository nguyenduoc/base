package com.dvn.base.audios.soundpool

import android.media.AudioManager
import android.media.SoundPool
import android.os.Build
import android.support.annotation.IdRes
import android.support.annotation.RawRes

import com.dvn.base.App

/**
 * Created by DVN on 5/7/2017.
 */

class SoundController private constructor() {

    private var mSoundPool: SoundPool? = null
    private var mSourceId = 0

    init {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mSoundPool = SoundPool.Builder().setMaxStreams(3)
                    .build()
        } else {
            mSoundPool = SoundPool(3, AudioManager.STREAM_MUSIC, 0)
        }
    }

    fun load(@RawRes sourceId: Int) {
        mSourceId = mSoundPool!!.load(App.get(), sourceId, 1)
    }

    fun play() {
        if (mSourceId == 0) return

        mSoundPool!!.play(mSourceId, 1f, 1f, 1, 0, 1f)
    }

    companion object {
        private var sInstance: SoundController? = null

        fun get(): SoundController? {
            if (sInstance == null) sInstance = SoundController()
            return sInstance
        }
    }
}
