package com.dvn.base.audios

/**
 * Created by Smile on 9/15/2016.
 */
interface AudioPlayingListener {
    fun playing(currentTime: Int)
}
