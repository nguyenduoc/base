package com.dvn.base.audios

/**
 * Created by D.V.N on 9/8/2016.
 */
interface IAudio {

    fun prepared()

    fun addPreparedListener(listener: AudioPreparedListener)

    fun setDataSource(url: String)

    fun start()

    fun start(url: String)

    fun startFromRaw(rawRes: Int)

    fun pause()

    fun resume()

    fun stop()

    val isPlaying: Boolean

    fun destroy()

    fun setAudioPlayingListener(listener: AudioPlayingListener)

    val duration: Int

    val isStarted: Boolean

    fun setCurrentPosition(position: Int)
}
