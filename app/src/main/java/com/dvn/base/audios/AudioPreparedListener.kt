package com.dvn.base.audios

/**
 * Created by D.V.N on 9/16/2016.
 */
interface AudioPreparedListener {
    fun onAudioPreparedFinish()
}
