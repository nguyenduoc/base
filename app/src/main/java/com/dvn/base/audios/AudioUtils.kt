package com.dvn.base.audios

import android.text.TextUtils

import com.dvn.base.App
import com.dvn.base.Config

import java.io.File

/**
 * Created by Smile on 4/28/2016.
 */
object AudioUtils {

    fun formatLink(path: String): String {
        if (TextUtils.isEmpty(path)) {
            return path
        }
        if (checkLinkAbsolutePath(path)) {
            return path
        }
        return Config.SERVER_AUDIO + path
    }


    fun sanitizeDirectory(name: String?): File? {
        var name: String? = name ?: return null

        if (checkLinkAbsolutePath(name)) {
            name = getPathFromLinkAbsolutePath(name!!)
        }
        val value = splitPath(name!!) ?: return null

        val dir = File(App.get()!!.filesDir.toString() + "/" + value[0])
        dir.mkdirs()

        return File(dir, value[1])
    }

    fun hasFile(name: String): Boolean {
        var name = name
        if (TextUtils.isEmpty(name)) {
            return false
        }

        if (checkLinkAbsolutePath(name)) {
            name = getPathFromLinkAbsolutePath(name)
        }
        val file = File(App.get()!!.filesDir.toString() + "/", name)
        return file.exists()
    }

    private fun splitPath(name: String): Array<String?>? {
        if (TextUtils.isEmpty(name))
            return null
        val value = arrayOfNulls<String>(2)
        for (i in name.length - 1 downTo 0) {
            if (name[i] == '/') {
                value[0] = name.substring(0, i)
                value[1] = name.substring(i + 1, name.length)
                return value
            }
        }
        value[0] = ""
        value[1] = name
        return value
    }

    fun getFileName(path: String): String? {
        val values = splitPath(path)
        if (values == null || values.size < 2) return null
        return values[1]
    }

    fun checkLinkAbsolutePath(name: String?): Boolean {
        if (name == null) {
            return false
        }
        if (name.startsWith("http")) {
            return true
        }
        return false
    }

    fun getPathFromLinkAbsolutePath(name: String): String {
        if (TextUtils.isEmpty(name)) {
            return ""
        }

        for (i in 0..name.length - 1) {
            val index = i + 3
            if (index < name.length) {
                val s = name.substring(i, index)
                if (s == "net" || s == "com" || s == ".tv") {
                    return name.substring(i + 3, name.length)
                }
            } else {
                return name
            }
        }
        return ""
    }
}
