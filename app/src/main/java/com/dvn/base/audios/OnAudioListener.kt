package com.dvn.base.audios

/**
 * Created by D.V.N on 9/8/2016.
 */
interface OnAudioListener {

    fun onAudioStart()

    fun onAudioPause()

    fun onAudioResume()

    fun onAudioStop()

    fun onAudioEnd()

    fun onAudioError()
}
