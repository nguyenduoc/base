package com.dvn.base

import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import com.dvn.base.navigations.ActivityNavigation
import com.dvn.base.navigations.INavigationController

abstract class DActivity : AppCompatActivity() {

    /**
     * check activity has save instance

     * @return true if activity has save instance state or if false not save instance
     */
    protected var isSaveInstanceState: Boolean = false
        private set
    var mNavigationController: INavigationController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isSaveInstanceState = false
        mNavigationController = ActivityNavigation(this, android.R.id.content)
    }

    override fun onStart() {
        super.onStart()
        isSaveInstanceState = false
    }

    override fun onRestart() {
        super.onRestart()
        isSaveInstanceState = false
    }

    override fun onResume() {
        super.onResume()
        isSaveInstanceState = false
    }


    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)
        isSaveInstanceState = true
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        isSaveInstanceState = true
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mNavigationController != null) mNavigationController!!.destroy()
    }

    fun canShowPage(): Boolean {
        return !isSaveInstanceState
    }
}
