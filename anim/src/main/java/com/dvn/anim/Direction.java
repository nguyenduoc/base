package com.dvn.anim;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.dvn.anim.Direction.DOWN;
import static com.dvn.anim.Direction.LEFT;
import static com.dvn.anim.Direction.RIGHT;
import static com.dvn.anim.Direction.UP;

/**
 * Created by DVN on 5/6/2017.
 */
@IntDef({UP, DOWN, LEFT, RIGHT})
@Retention(RetentionPolicy.SOURCE)
@interface Direction {
    int UP = 1;
    int DOWN = 2;
    int LEFT = 3;
    int RIGHT = 4;
}
