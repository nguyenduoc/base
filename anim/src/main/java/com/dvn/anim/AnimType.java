package com.dvn.anim;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.dvn.anim.AnimType.FLIP_DOWN;
import static com.dvn.anim.AnimType.FLIP_LEFT;
import static com.dvn.anim.AnimType.FLIP_RIGHT;
import static com.dvn.anim.AnimType.FLIP_UP;
import static com.dvn.anim.AnimType.MOVE_DOWN;
import static com.dvn.anim.AnimType.MOVE_LEFT;
import static com.dvn.anim.AnimType.MOVE_RIGHT;
import static com.dvn.anim.AnimType.MOVE_UP;
import static com.dvn.anim.AnimType.NONE;
import static com.dvn.anim.AnimType.SLIDE_DOWN;
import static com.dvn.anim.AnimType.SLIDE_LEFT;
import static com.dvn.anim.AnimType.SLIDE_RIGHT;
import static com.dvn.anim.AnimType.SLIDE_UP;

/**
 * Created by DVN on 5/6/2017.
 */
@IntDef({NONE,
        FLIP_LEFT, FLIP_UP, FLIP_RIGHT, FLIP_DOWN,
        SLIDE_LEFT, SLIDE_UP, SLIDE_RIGHT, SLIDE_DOWN,
        MOVE_LEFT, MOVE_UP, MOVE_RIGHT, MOVE_DOWN})
@Retention(RetentionPolicy.SOURCE)
public @interface AnimType {
    int NONE = 0;

    int FLIP_LEFT = -1;
    int FLIP_RIGHT = 1;
    int FLIP_UP = -2;
    int FLIP_DOWN = 2;

    int SLIDE_LEFT = 5;
    int SLIDE_UP = 6;
    int SLIDE_RIGHT = 7;
    int SLIDE_DOWN = 8;

    int MOVE_LEFT = 9;
    int MOVE_UP = 10;
    int MOVE_RIGHT = 11;
    int MOVE_DOWN = 12;
}

