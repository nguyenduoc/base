package com.dvn.anim;

import android.view.animation.Transformation;

/**
 * Created by DVN on 5/6/2017.
 */

public class FlipAnimation {

    static class FlipLeftAnimation extends FragmentAnimation {


        FlipLeftAnimation(boolean enter, int duration) {
            super(enter, duration);
        }

        @Override
        public void initialize(int width, int height, int parentWidth, int parentHeight) {
            super.initialize(width, height, parentWidth, parentHeight);
            mPivotY = height * 0.5F;
            mPivotX = !mEnter ? 0.0f : width;
            mCameraZ = -width * 0.015f;
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            float value = mEnter ? (interpolatedTime - 1.0f) : interpolatedTime;
            value *= -1.0f;
            mRotationY = -value * 180.0f;
            mTranslationX = -value * mWidth;

            super.applyTransformation(interpolatedTime, t);

            // Hide entering/exiting view before/after half point.
            if (mEnter) {
                mAlpha = interpolatedTime <= 0.5f ? 0.0f : 1.0f;
            } else {
                mAlpha = interpolatedTime <= 0.5f ? 1.0f : 0.0f;
            }

            applyTransformation(t);
        }
    }


    public static class FlipRightAnimation extends FragmentAnimation {


        public FlipRightAnimation(boolean enter, int duration) {
            super(enter, duration);
        }

        @Override
        public void initialize(int width, int height, int parentWidth, int parentHeight) {
            super.initialize(width, height, parentWidth, parentHeight);
            mPivotY = height * 0.5F;
            mPivotX = mEnter ? 0.0f : width;
            mCameraZ = -width * 0.015f;
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            float value = mEnter ? (interpolatedTime - 1.0f) : interpolatedTime;
            value *= -1.0f;
            mRotationY = value * 180.0f;
            mTranslationX = value * mWidth;

            super.applyTransformation(interpolatedTime, t);

            // Hide entering/exiting view before/after half point.
            if (mEnter) {
                mAlpha = interpolatedTime <= 0.5f ? 0.0f : 1.0f;
            } else {
                mAlpha = interpolatedTime <= 0.5f ? 1.0f : 0.0f;
            }

            applyTransformation(t);
        }
    }
}
