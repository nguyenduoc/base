package com.dvn.anim;

import android.view.animation.Animation;

/**
 * Created by DVN on 5/6/2017.
 */

public class AnimController {
    private static final int DURATION_DEFAULT = 700;

    public static Animation createAnimation(@AnimType int type, boolean enter, boolean restore) {
        switch (type) {
            case AnimType.FLIP_LEFT:
                if (restore && enter)
                    return new FlipAnimation.FlipRightAnimation(enter, DURATION_DEFAULT);
                else
                    return new FlipAnimation.FlipLeftAnimation(enter, DURATION_DEFAULT);

            case AnimType.FLIP_RIGHT:
                if (restore && enter)
                    return new FlipAnimation.FlipLeftAnimation(enter, DURATION_DEFAULT);
                else
                    return new FlipAnimation.FlipRightAnimation(enter, DURATION_DEFAULT);

            case AnimType.MOVE_LEFT:
                if (restore && enter)
                    return new MoveAnimation.MoveRightAnimation(DURATION_DEFAULT, enter);
                else
                    return new MoveAnimation.MoveLeftAnimation(DURATION_DEFAULT, enter);

            case AnimType.MOVE_RIGHT:
                return new MoveAnimation.MoveRightAnimation(DURATION_DEFAULT, enter);
            case AnimType.FLIP_DOWN:
            default:
                break;
        }
        return null;
    }

    @AnimType
    public static int getAnimTypeReverse(@AnimType int type) {
        switch (type) {
            case AnimType.FLIP_LEFT:
                return AnimType.FLIP_RIGHT;
            case AnimType.FLIP_DOWN:
                return AnimType.FLIP_UP;
            case AnimType.FLIP_RIGHT:
                return AnimType.FLIP_LEFT;
            case AnimType.FLIP_UP:
                return AnimType.FLIP_DOWN;

            case AnimType.MOVE_DOWN:
                return AnimType.MOVE_UP;
            case AnimType.MOVE_LEFT:
                return AnimType.MOVE_RIGHT;
            case AnimType.MOVE_RIGHT:
                return AnimType.MOVE_LEFT;
            case AnimType.MOVE_UP:
                return AnimType.MOVE_DOWN;

            case AnimType.SLIDE_DOWN:
                return AnimType.SLIDE_UP;
            case AnimType.SLIDE_LEFT:
                return AnimType.SLIDE_RIGHT;
            case AnimType.SLIDE_RIGHT:
                return AnimType.SLIDE_LEFT;
            case AnimType.SLIDE_UP:
                return AnimType.SLIDE_DOWN;
            case AnimType.NONE:
                break;
        }
        return AnimType.NONE;
    }

}
