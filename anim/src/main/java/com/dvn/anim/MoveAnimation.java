package com.dvn.anim;

import android.view.animation.Transformation;

/**
 * Created by DVN on 5/6/2017.
 */

class MoveAnimation {

    static class MoveLeftAnimation extends FragmentAnimation {

        MoveLeftAnimation(int duration, boolean enter) {
            super(enter, duration);
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            float value = mEnter ? (interpolatedTime - 1.0f) : interpolatedTime;
            mTranslationX = -value * mWidth;
            super.applyTransformation(interpolatedTime, t);
            applyTransformation(t);
        }

    }

    static class MoveRightAnimation extends FragmentAnimation {

        MoveRightAnimation(int duration, boolean enter) {
            super(enter, duration);
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            float value = mEnter ? (interpolatedTime - 1.0f) : interpolatedTime;
            mTranslationX = value * mWidth;

            super.applyTransformation(interpolatedTime, t);
            applyTransformation(t);
        }

    }
}
