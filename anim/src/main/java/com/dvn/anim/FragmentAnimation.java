package com.dvn.anim;

/**
 * Created by DVN on 5/6/2017.
 */

class FragmentAnimation extends DAnimation {
    boolean mEnter;

    FragmentAnimation(boolean enter, int duration) {
        this.mEnter = enter;
        setDuration(duration);
    }
}
